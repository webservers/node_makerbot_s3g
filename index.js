var http = require('http');
var fs = require('fs');
var s3g = require('./s3g.js');
//var io = require('socket.io').listen(3002);
var tty = require("serialport");
var querystring = require('querystring');

//console.log(JSON.stringify(t.CONSTANTS));
//now the socketio server
var serialPort = null;
var decoder = new s3g.PacketStreamDecoder();
tty.list(function (err, ports) {
	if(err) throw err;
	console.log("#ports: "+ports.length);
	for(var i = 0; i < ports.length; ++i) {  
		if(ports[i].pnpId.indexOf('MakerBot') !== -1 || ports[i].manufacturer.indexOf('MakerBot Industries') !== -1) {
			console.log("Found MakerBot", ports[i].comName);
			serialPort = new tty.SerialPort(ports[i].comName, {
				baudrate: 9600
				//,bufferSize: 4096
			});
			
			serialPort.on("open", function () {
				var protocal_packet = s3g.HOST_CMD.INIT();
				//var protocal_packet = s3g.HOST_CMD.RESET();
				//var protocal_packet = s3g.HOST_CMD.TOOL_QUERY(0, s3g.CONSTANTS.TOOL.QUERY.GET_TOOLHEAD_TEMP);
				serialPort.write(protocal_packet, function(err, bytes_written) {
					if (err) console.log('error: ' + err);
					console.log('bytes_written: ' + bytes_written);
				});
				serialPort.on('data', function(data) {
					console.log('data:' + data);
					console.log('decoded: ' + decoder(data));
					//var a = data.toString().split(/\n/);
					//serialPort.write("ls\n", function(err, results) {
					//  console.log('err ' + err);
					//  console.log('results ' + results);
					//});
				});
			});
		} else {
			console.log(ports[i].pnpId);
		}
	}
});

/*
io.on('connection', function (socket) {
	console.log("new connection...");
);	
    socket.emit('log', {
        tag: "debug",
        message: "begin session..."
    });
});
*/

process.on('SIGINT', function() {    
	 if(serialPort != null && serialPort.isOpen()){
	 	serialPort.close();
	 	console.log("Closing Com Port...");
	 }
    process.exit();
});

/*
http.createServer(function(req, resp){
	switch(req.method){
		case "GET":
			//console.log(req.url);
			if(req.url.search("public") != -1){
				var file_name = req.url.substring(1, req.url.length);
				var extension = file_name.substring(file_name.indexOf('.'), file_name.length);
				//console.log(file_name);
				if(extension == '.htm' || extension == '.js' || extension == '.css'){						
					//resp.writeHead(200, {"Content-Type": "text/html"});
					fs.readFile(file_name, 'utf8', function (err, data) {
					  if (err) {
						return console.log(err);
					  }
						//console.log(data);
						resp.write(data);
						resp.end();				  
					});
				} else {
					//resp.writeHead(200, {"Content-Type": "text/html"});
					fs.readFile(file_name, function (err, data) {
					  if (err) {
						return console.log(err);
					  }
						//console.log(data);
						resp.write(data);
						resp.end();				  
					});					
				}
			}
			else if(req.url === "/json"){
				resp.writeHead(200, {"Content-Type": "application/json"});
				resp.write({});
				//console.log(d);
				resp.end();
			}
			break;
		case "POST":
			console.log(req.url);
			var body = "";
			req.on('data', function (data) {
				body += data;
				// Too much POST data, kill the connection!
				// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
				if (body.length > 1e6)
					req.connection.destroy();
			});

			req.on('end', function () {
				//var post = querystring.parse(body);
				console.log(body);
			});				
			break;
	}
});
*/