//return unix timeStamp
function unix_time(){
	return Math.round((new Date()).getTime() / 1000);
}

function isEven(someNumber){
    return (someNumber%2 == 0) ? true : false;
}
function isDefined(v){
	if(typeof(v) == 'undefined')
		return false;
	return true;
}
function notNull(v){
    if(isDefined(v) && v != null)
        return true;
    return false;
}
function isArray(v){
    if(notNull(v) && typeof(v.length) == 'number' && v.length > 0)
        return true;
    return false;
}
function isString(v){
    if(notNull(v) && typeof(v.charAt) == 'function')
        return true;
    return false;
}
function isNum(v){
    if(notNull(v) && typeof(v) == 'number')
        return true;
    return false;
}
function isTrue(v){
	if(typeof(v) == 'string'){
		if(v.toLowerCase() == "true")
			return true;
	} else if(typeof(v) == 'boolean' && v)
		return v;
	return false;	
}
function isFunction(v){
	if(notNull(v) && typeof(v) == 'function')
		return true;
	return false;	
}
function isObject(v){
	if(notNull(v) && typeof(v) == "object")
		return true;
	return false;
}
function isArray(v){
    if(notNull(v) && isFunction(v.slice))
        return true;
    return false;
}


function dump(e){
	var str = "";
	if(typeof e == 'object'){
		for(key in e){
			console.log("key: "+key+", value:"+e[key]);
		}
	}
	else console.log("Value: "+e);
	return str;
}

/* Strip Chars Usage-------------------------------------------
var StripAlpha = Strip.Chars('A-Z');
var StripNumeric = Strip.Chars('0-9');
var StripSymbols = Strip.Chars('**');
var StripCustom = Strip.Chars('Characters I want removed!');

console.log(StripAlpha.FromString("@4Te*st1"));   = @4*1
console.log(Strip.Chars('A-Z**').FromString("@4Te*st1")); = 41
console.log(Strip.Chars('0-9*').FromString("@4Te*st1"));  = @Test
console.log(Strip.Chars('A-Z*').FromString("@4Te*st1"));  = @41
 */
var Strip = {
	invalidChars: '',
	//var invalidSequance = ["\U00a5","\U00a3","\U20ac"]; //these are Unicode char sequances
	FromString: function(CharString){
		var OutputString = CharString;
		var i = 0;
		invalidChars = this.invalidChars;
		//console.log(invalidChars);
	    function clearInvalidChar(){
			var OffendingCharAtPosition = OutputString.indexOf(invalidChars[i]);	
			if(OffendingCharAtPosition != -1){   		
		      if(OffendingCharAtPosition == (OutputString.length-1))
	    		  OutputString = OutputString.slice(0, (OffendingCharAtPosition));
			  else if(OffendingCharAtPosition == 0)
	    		  OutputString = OutputString.slice(1);  	
			  else if(OffendingCharAtPosition < (OutputString.length-1)){
	    		  var ls = OutputString.slice(0, (OffendingCharAtPosition));
	    		  OutputString = ls + OutputString.slice(OffendingCharAtPosition+1);
	    	   }
	    	   return true;
	        }
	        return false;
	    }
		for(i=0;i<invalidChars.length;i++){
	        while( clearInvalidChar() ){
	         //loop through until all existance of current invalid char is removed
	        }
		}
		return OutputString;
	},
	Chars: function(_chars){
	   this.invalidChars = '';
	   Numbers =  "0123456789";
	   AlphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   Symbols = "` []{}#%^*+=_|~<>.,?!'-:;()$&@/\\"+'"';
	   if(typeof(_chars) == 'string'){
	    if(_chars.indexOf("A-Z") != -1){
	       this.invalidChars += AlphaUpper+AlphaUpper.toLowerCase();
	    		  var ls = _chars.slice(0, (_chars.indexOf("A-Z")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("A-Z")+3);	       
	       //console.log(_chars);
	    }
	    if(_chars.indexOf("0-9") != -1){
	    		  var ls = _chars.slice(0, (_chars.indexOf("0-9")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("0-9")+3);
	    		  //console.log(_chars);
	       this.invalidChars += Numbers;
	    }
	    if(_chars.indexOf("**") != -1){
	    		  var ls = _chars.slice(0, (_chars.indexOf("**")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("**")+2);	             
	             //console.log(_chars);
	       this.invalidChars += Symbols;
	    }
	    this.invalidChars += _chars;
	   }
	   return this;
	}
};
//Remove All But Numeric
function Numeric(e){
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('A-Z**').FromString(e.source.value);
		}
	}
}
//Remove All But AlphaNumeric
function AlphaNumeric(e){
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('**').FromString(e.source.value);
		}
	}
}
//Remove All But PhoneChars
function Phone(e){
	Symbols = "`[]{}#%^*+=_|~<>.,?!':;$&@/\\"+'"';
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('A-Z'+Symbols).FromString(e.source.value);
		}
	}
}


function stripslashes (str) {
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Ates Goral (http://magnetiq.com)
  // +      fixed by: Mick@el
  // +   improved by: marrtins
  // +   bugfixed by: Onno Marsman
  // +   improved by: rezna
  // +   input by: Rick Waldron
  // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
  // +   input by: Brant Messenger (http://www.brantmessenger.com/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: stripslashes('Kevin\'s code');
  // *     returns 1: "Kevin's code"
  // *     example 2: stripslashes('Kevin\\\'s code');
  // *     returns 2: "Kevin\'s code"
  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}