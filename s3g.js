 /*
 
 https://github.com/makerbot/s3g/blob/master/doc/s3gProtocol.md
 
 */

  var CONSTANTS = {
    PROTOCOL_STARTBYTE    : 0xD5,
    MAX_PAYLOAD_LENGTH    : 32, //? where this from
    HOST : {
		QUERY : {
			'GET_VERSION'	  	: 0,
			'INIT'			  	: 1,
			'GET_AVAILABLE_BUFFER_SIZE'	: 2,
			'CLEAR_BUFFER'		: 3,
			'ABORT_IMMEDIATELY' : 7,
			'PAUSE_RESUME' 		: 8,
			'TOOL_QUERY'      	: 10,
			'IS_FINISHED'		: 11,
			'READ_EEPROM'		: 12,
			'WRITE_EEPROM'		: 13,
			'CAPTURE_TO_FILE'	: 14,
			'END_CAPTURE_TO_FILE'		: 15,
			'PLAY_FILE'			: 16,
			'RESET'				: 17,
			'GET_NEXT_FILENAME'	: 18,
			'GET_BUILD_NAME'  	: 20,
			'GET_EXTENDED_POSITION'		: 21,
			'EXTENDED_STOP'		: 22,
			'GET_BOARD_STATUS'	: 23,
			'GET_BUILD_STATS' 	: 24,
			'GET_COMMUNICATION_STATS'	: 25,
			'GET_ADVANCED_VERSION'		: 27
		},
		BUFFERED_QUERY : {
			'FIND_AXIS_MINIMUMS'	: 131,
			'FIND_AXIS_MAXIMUMS'	: 132,
			'DELAY'					: 133,
			'CHANGE_TOOL'			: 134,
			'WAIT_TOOL_READY'		: 135,
			'TOOL_ACTION'			: 136,
			'ENABLE_DISABLE_AXIS'	: 137,
			'QUEUE_EXTENDED_POINT'	: 139,
			'SET_EXTENDED_POSITION'	: 140,
			'WAIT_PLATFORM_READY'	: 141,
			'QUEUE_EXTENDED_POINT_NEW_STYLE'	: 142,
			'STORE_HOME_POSITIONS'	: 143,
			'RECALL_HOME_POSITIONS'	: 144,
			'SET_DIGITAL_POT'		: 145,
			'SET_RGB_LED'			: 146,
			'SET_BEEP'				: 147,
			'WAIT_FOR_BUTTON'		: 148,
			'DISPLAY_LCD_MSG'		: 149,
			'SET_BUILD_PERCENTAGE'	: 150,
			'QUEUE_SONG'			: 151,
			'FACTORY_RESET'			: 152,
			'BUILD_START_NOTIFICATION'	: 153,
			'BUILD_END_NOTIFICATION': 154, 
			'QUEUE_POINT_X3G'		: 155,
			'STREAM_VERSION'		: 157
		},
		VERSION_ID: {
			'MAKERBOT_OFFICIAL'	: 0x01,
			'SAILFISH'			: 0x80
		},
		STREAM_VERSIONS: {
			'REPLICATOR'	: 0xD314,
			'REPLICATOR2'	: 0xB015
		},
		BUTTON_BITFEILD: {
			'CENTER'	: 0,
			'RIGHT'		: 1,
			'LEFT'		: 2,
			'DOWN'		: 3,
			'UP'		: 4,
			'RESET'		: 5
		},
		BUTTON_OPTIONS:	{
			'CHANGE_READY_STATE_ON_TIMEOUT'	: 0,
			'RESET_BOT_ON_TIMEOUT'			: 1,
			'CLEAR_SCREEN_ON_PRESS'			: 2
		},
		BOARD_STATUS: {
			'PREHEAT'			: 0,
			'MANUAL_MODE'		: 1,
			'ONBOARD_SCRIPT'	: 2,
			'ONBOARD_PROCESS'	: 3,
			'WAIT_FOR_BUTTON'	: 4,
			'BUILD_CANCELLING'	: 5,
			'HEAT_SHUTDOWN'		: 6,
			'POWER_ERRPR'		: 7
		},
		LCD_OPTIONS: {
			'CLEAR_EXISTING_MESSAGE'	: 0,
			'LAST_MSG_GROUP'			: 1,
			'WAIT_FOR BUTTON_PRESS'		: 2
		}
    },
    TOOL : {
		QUERY : {
			'GET_VERSION'				: 0,
			'GET_TOOLHEAD_TEMP'       	: 2,
			'GET_RPM'					: 17,
			'IS_TOOL_READY'				: 22,
			'READ_EEPROM'				: 25,
			'WRITE_EEPROM'				: 26,
			'GET_PLATFORM-TEMP'			: 30,
			'GET_TOOLHEAD_TARGET_TEMP'	: 32,
			'GET_PLATFORM_TARGET_TEMP'	: 33,
			'IS_PLATFORM_READY'			: 35,
			'GET_TOOL_STATUS'			: 36,
			'GET_PID_STATE'				: 37
		},
		ACTION_QUERY: {
			'INIT'						: 01,
			'SET_TOOLHEAD_TARGET_TEMP'	: 03,
			'SET_RPM'					: 06,
			'ENABLE_DISABLE_MOTOR'		: 10,
			'ENABLE_DISABLE_FAN'		: 12,
			'ENABLE_DISABLE_EXTRA_OUTPUT'	: 13,
			'SET_SERVO1_POSITION'		: 14,
			'PAUSE_RESUME'				: 23,
			'ABORT_IMMEDIATELY'			: 24,
			'SET_PLATFORM_TARGET_TEMP'	: 31
			
		},
		DEPRICATED_TOOL_STATUS:	{
			'EXTRUDER_READY'		: 0,
			//reserved for future use : 1
			'POWER_ON_RESET'		: 2,
			'EXTERNAL_RESET'		: 3,
			'BROWN_OUT_RESET'		: 4,
			'WATCHDOG_RESET'		: 5,
			'PLATFORM_ERROR'		: 6,
			'EXTRUDER_ERROR'		: 7
		},
		TOOL_STATUS:	{
			'EXTRUDER_READY'		: 0,
			'UNPLUGGED'				: 1,
			'Software Cutoff'		: 2,
			'NOT_HEATING'			: 3,
			//reserved for future use : 5,
			'TEMP_DROPPING'			: 4,
			'PLATFORM_ERROR'		: 6,
			'EXTRUDER_ERROR'		: 7
		}
    },
    RESPONSE_CODE : {
      'GENERIC_PACKET_ERROR'  : 0x80,
      'SUCCESS'               : 0x81,
      'ACTION_BUFFER_OVERFLOW': 0x82,
      'CRC_MISMATCH'          : 0x83,
      'PACKET_TOO_BIG'		  : 0x85,
      'DOWNSTREAM_TIMEOUT'    : 0x87,
      'TOOL_LOCK_TIMEOUT'     : 0x88,
      'CANCEL_BUILD'          : 0x89,
      'ACTIVE_SD_BUILD'    	  : 0x8A,
      'OVERHEAT_STATE'        : 0x8B,
	  'PACKET_TIMEOUT'		  : 0x8C
    },
    BUILD_STATE: {
      'NO_BUILD_INITIALIZED'    : 0x00,
      'BUILD_RUNNING'           : 0x01,
      'BUILD_FINISHED_NORMALLY' : 0x02,
      'BUILD_PAUSED'            : 0x03,
      'BUILD_CANCELLED'         : 0x04,
      'BUILD_SLEEPING'          : 0x05
    },
	SD_RESPONSE_CODE: {
		'SUCCESS'				: 0x00,
		'NOT_PRESENT'			: 0x01,
		'INIT_FAILURE'			: 0x02,
		'PARTITION_TABLE_ERROR'	: 0x03,
		'FILESYSTEM_ERROR'		: 0x04,
		'ROOT_DIR_ERROR'		: 0x05,
		'SD_LOCKED'				: 0x06
	},
	AXES_BITFIELD: {
		x: 0,
		y: 1,
		z: 2,
		a: 3,
		b: 4
	}
  };

  function exception(name, message) {
    return {"name":name, "message":message};
  }
  
  var types = ['uint8', 'uint16', 'ascii', 'int32', 'uint32'];
  
  /**
   * CRC table from http://forum.sparkfun.com/viewtopic.php?p=51145
   */
  var crctab = [
      0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
      157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
      35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
      190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
      70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
      219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
      101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
      248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
      140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
      17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
      175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
      50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
      202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
      87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
      233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
      116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
  ];
  
  /**
  * Calculate 8-bit [iButton/Maxim CRC][http://www.maxim-ic.com/app-notes/index.mvp/id/27] of the payload
  * @method CRC
  * @param {ArrayBuffer} payload
  * @return {uint8} Returns crc value on success, throws exceptions on failure
  */
 function CRC(payload) {
    if(!payload) {
      throw exception("Argument Exception", 'payload is null or undefined');
    } else if (!(payload instanceof ArrayBuffer)) {
      throw exception("Argument Exception", 'payload is not an ArrayBuffer');
    }
    var crc = 0;
    for(var i = 0; i < payload.byteLength; ++i) {
      crc = crctab[crc ^ payload[i]];
    }
    return crc;
  };  
  
  
   /**
  * Create protocol message from ArrayBuffer
  *
  * @method encode
  * @param {ArrayBuffer} payload Single Payload of s3g Protocol Message
  * @return {ArrayBuffer} Returns packet on success, throws exceptions on failure
  */
  function PacketStreamEncoder(payload) {
    if(!payload) {
      throw exception("Argument Exception", 'payload is null or undefined');
    } else if (!(payload instanceof ArrayBuffer)) {
      throw exception("Argument Exception", 'payload is not an ArrayBuffer');
    } else if (payload.byteLength > CONSTANTS.MAX_PAYLOAD_LENGTH) {
      throw exception("Packet Length Exception", 'payload length (' + payload.byteLength + ') is greater than max ('+ CONSTANTS.MAX_PAYLOAD_LENGTH + ').');
    }

    // Create Packet
    var len = payload.byteLength,
    packet = new DataView(new ArrayBuffer(len + 3 /* Protocol Bytes */));
    packet.setUint8(0, CONSTANTS.PROTOCOL_STARTBYTE);
    packet.setUint8(1, len);

    for(var i = 0, offset = 2; i < payload.byteLength; ++i, ++offset) {
      packet.setUint8(offset, payload[i]);
    }
    packet.setUint8(len + 2, CRC(payload));
    return packet;
  }
  
 
  function PacketStreamDecoder() {
    this.payload = undefined;
    this.payloadOffset = 0;
    this.expectedLength = 0;
	this.STATES = {
		WAIT_FOR_HEADER: 0,
		WAIT_FOR_LENGTH: 1,
		WAIT_FOR_DATA  : 2,
		WAIT_FOR_CRC   : 3,
		PAYLOAD_READY  : 4
	};	
	
	this.state = this.STATES.WAIT_FOR_HEADER;
	  /**
	   * Re-construct Packet one byte at a time
	   * @param _byte Byte to add to the stream
	   */
	  this.parseByte = function (_byte) {
		switch(this.state) {
		  case this.STATES.WAIT_FOR_HEADER:
			if(_byte !== CONSTANTS.PROTOCOL_STARTBYTE) {
			  throw _exception('Packet Header Exception', 'packet header value incorrect('+_byte+')');
			}
			this.state = this.STATES.WAIT_FOR_LENGTH;
			break;
		  
		  case this.STATES.WAIT_FOR_LENGTH:
			if (_byte > CONSTANTS.MAX_PAYLOAD_LENGTH) {
			  throw _exception('Packet Length Exception', 'packet length ('+ _byte +') value greater than max.');
			}
			this.expectedLength = _byte;
			this.state = this.STATES.WAIT_FOR_DATA;
			break;

		  case this.STATES.WAIT_FOR_DATA:
			if (!this.payload) {
			  this.payload = new ArrayBuffer(this.expectedLength);
			}
			this.payload[this.payloadOffset] = _byte;
			++this.payloadOffset;
			if (this.payloadOffset > this.expectedLength) {
			  throw _exception('Packet Length Exception', 'packet length incorrect.');
			} else if (this.payloadOffset === this.expectedLength) {
			  this.state = this.STATES.WAIT_FOR_CRC;
			}
			break;
		  case this.STATES.WAIT_FOR_CRC:
			var crc = CRC(this.payload);
			if (crc !== _byte){
			  throw _exception('Packet CRC Exception', 'packet crc incorrect.');
			}
			this.state = this.STATES.PAYLOAD_READY;
			break;
		  default:
			throw _exception('Parser Exception', 'default state reached.');
		}
	  };
	  
	  this.reset = function() {
		this.state = this.STATES.WAIT_FOR_HEADER;
		this.payload = undefined;
		this.payloadOffset = 0;
		this.expectedLength = 0;
	  };

	  this.isPayloadReady = function() {
		return this.state === this.STATES.PAYLOAD_READY;
	  };
  }


  function BuildPacketStream() {
    var payload = new ArrayBuffer(arguments.length);
    for (var i = 0; i < arguments.length; ++i) {
      payload[i] = arguments[i];
    }
    var packet = PacketStreamEncoder(payload);
    var buffer = new Buffer(packet.byteLength, false);
    for (var i = 0; i < packet.byteLength; ++i) {
      buffer[i] = packet[i];
    }
    return buffer;
  }
  
  var HOST_CMD = {
		'GET_VERSION': function(host_version_uint16){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_VERSION, host_version_uint16);
		},
		'INIT': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.INIT);
		},
		'GET_AVAILABLE_BUFFER_SIZE': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_AVAILABLE_BUFFER_SIZE);
		},
		'CLEAR_BUFFER': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.CLEAR_BUFFER);
		},
		'ABORT_IMMEDIATELY': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.ABORT_IMMEDIATELY);
		},
		'PAUSE_RESUME': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.PAUSE_RESUME);
		},
		'TOOL_QUERY': function(tool_index_uint8, query){
			if(typeof(query) != 'undefined')
				return BuildPacketStream(CONSTANTS.HOST.QUERY.TOOL_QUERY, tool_index_uint8, query);
			return BuildPacketStream(CONSTANTS.HOST.QUERY.TOOL_QUERY, tool_index_uint8);
		},
		'IS_FINISHED': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.IS_FINISHED);
		},
		'READ_EEPROM': function(offset_uint16, length_uint8){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.READ_EEPROM, offset_uint16, length_uint8);
		},
		'WRITE_EEPROM': function(offset_uint16, length_uint8, bytes){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.WRITE_EEPROM, offset_uint16, length_uint8, bytes);
		},
		'CAPTURE_TO_FILE': function(filename_ascii){
			if(filename_ascii.length <= 12)
				return BuildPacketStream(CONSTANTS.HOST.QUERY.CAPTURE_TO_FILE, filename_ascii);
			console.error("file name > 12");
		},
		'END_CAPTURE_TO_FILE': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.END_CAPTURE_TO_FILE);
		},
		'PLAY_FILE': function(filename_ascii){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.PLAY_FILE, filename_ascii);
		},
		'RESET': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.RESET);
		},
		'GET_NEXT_FILENAME': function(filename_ascii){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_NEXT_FILENAME, filename_ascii);
		},
		'GET_BUILD_NAME': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_BUILD_NAME);
		},
		'GET_EXTENDED_POSITION': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_EXTENDED_POSITION);
		},
		'EXTENDED_STOP': function(bitfield_uint8){
			//If bit 0 is set, halt all stepper motion. If bit 1 is set, clear the command queue.
			return BuildPacketStream(CONSTANTS.HOST.QUERY.EXTENDED_STOP, bitfield_uint8);
		},
		'GET_BOARD_STATUS': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_BOARD_STATUS);
		},
		'GET_BUILD_STATS': function(){
		//Payload (1 byte)
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_BUILD_STATS);
		},
		'GET_COMMUNICATION_STATS': function(){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_COMMUNICATION_STATS);
		},
		'GET_ADVANCED_VERSION': function(host_version_uint16){
			return BuildPacketStream(CONSTANTS.HOST.QUERY.GET_ADVANCED_VERSION, host_version_uint16);
		},

//		BUFFERED
		'FIND_AXIS_MINIMUMS': function(axes_bitfield_uint8, feedrate_uint32, timeout_uint16){
			return BuildPacketStream(CONSTANTS.HOST.BUFFERED_QUERY.FIND_AXIS_MINIMUMS, axes_bitfield_uint8, feedrate_uint32, timeout_uint16);	
		},
		'FIND_AXIS_MAXIMUMS': function(axes_bitfield_uint8, feedrate_uint32, timeout_uint16){
			return BuildPacketStream(CONSTANTS.HOST.BUFFERED_QUERY.FIND_AXIS_MAXIMUMS, axes_bitfield_uint8, feedrate_uint32, timeout_uint16);	
		},
		'DELAY': function(delay_uint32){
			return BuildPacketStream(CONSTANTS.HOST.BUFFERED_QUERY.DELAY, delay_uint32);		
		},
		'CHANGE_TOOL': function(tool_id_uint8){
			return BuildPacketStream(CONSTANTS.HOST.BUFFERED_QUERY.CHANGE_TOOL, tool_id_uint8);	
		},
		'WAIT_TOOL_READY': function(tool_id_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.WAIT_TOOL_READY
		},
'TOOL_ACTION': function(tool_id_uint8, action_uint8, length_uint8, command){
			//CONSTANTS.HOST.BUFFERED_QUERY.TOOL_ACTION	
		},
		'ENABLE_DISABLE_AXIS': function(axes_bitfield_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.ENABLE_DISABLE_AXIS	
		},
		'QUEUE_EXTENDED_POINT': function(x_int32, y_int32, z_int32, a_int32, b_int32, feedrate_uint32){
			//CONSTANTS.HOST.BUFFERED_QUERY.QUEUE_EXTENDED_POINT	
		},
		'SET_EXTENDED_POSITION': function(x_int32, y_int32, z_int32, a_int32, b_int32){
			//CONSTANTS.HOST.BUFFERED_QUERY.SET_EXTENDED_POSITION	
		},
		'WAIT_PLATFORM_READY': function(tool_id_uint8, delay_uint16, timeout_uint16){
			//CONSTANTS.HOST.BUFFERED_QUERY.WAIT_PLATFORM_READY	
		},
		'QUEUE_EXTENDED_POINT_NEW_STYLE': function(x_int32, y_int32, z_int32, a_int32, b_int32, duration_uint32, axes_bitfield_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.QUEUE_EXTENDED_POINT_NEW_STYLE		
		},
		'STORE_HOME_POSITIONS': function(axes_bitfield_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.STORE_HOME_POSITIONS		
		},
		'RECALL_HOME_POSITIONS': function(axes_bitfield_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.RECALL_HOME_POSITIONS		
		},
		'SET_DIGITAL_POT': function(axis_uint8, value_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.SET_DIGITAL_POT	
		},
		'SET_RGB_LED': function(red_uint8, green_uint8, blue_uint8, blink_rate_uint8){
			//(CONSTANTS.HOST.BUFFERED_QUERY.SET_RGB_LED, red_uint8, green_uint8, blue_uint8, blink_rate_uint8, 0);
		},
		'SET_BEEP': function(freq_uint16, length_uint16){
			//(CONSTANTS.HOST.BUFFERED_QUERY.SET_BEEP, freq_uint16, length_uint16, 0);	
		},
		'WAIT_FOR_BUTTON': function(button_bitfield_uint8, timeout_uint16, options_bitfield_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.WAIT_FOR_BUTTON		
		},
		'DISPLAY_LCD_MSG': function(options_bitfield_uint8, x_uint8, y_uint8, timeout_uint8, ascii){
			//CONSTANTS.HOST.BUFFERED_QUERY.DISPLAY_LCD_MSG		
		},
		'SET_BUILD_PERCENTAGE': function(percent_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.SET_BUILD_PERCENTAGE, percent_uint8, 0);		
		},
		'QUEUE_SONG': function(song_id_uint8){
			//CONSTANTS.HOST.BUFFERED_QUERY.QUEUE_SONG		
		},
		'FACTORY_RESET': function(){
			//CONSTANTS.HOST.BUFFERED_QUERY.FACTORY_RESET, 0);		
		},
		'BUILD_START_NOTIFICATION': function(ascii){
			//CONSTANTS.HOST.BUFFERED_QUERY.BUILD_START_NOTIFICATION, 0, ascii);		
		},
		'BUILD_END_NOTIFICATION': function(){
			//CONSTANTS.HOST.BUFFERED_QUERY.BUILD_END_NOTIFICATION, 0);		
		},
		'QUEUE_POINT_X3G': function(x_int32, y_int32, z_int32, a_int32, b_int32, feedrate_uint32, axes_bitfield_uint8, feedrate_uint16){
			//CONSTANTS.HOST.BUFFERED_QUERY.QUEUE_POINT_X3G		
		},
		'STREAM_VERSION': function(x3g_high_byte_uint8, x3g_low_byte_uint8, bot_type_uint16){
			//CONSTANTS.HOST.BUFFERED_QUERY.STREAM_VERSION, x3g_high_byte_uint8, x3g_low_byte_uint8, 0, 0, bot_type_uint16);		
		}
  };
  var TOOL_CMD = {

  };
module.exports.CONSTANTS = CONSTANTS;
module.exports.PacketStreamDecoder = PacketStreamDecoder;
//module.exports.BuildPacketStream = BuildPacketStream;
module.exports.HOST_CMD = HOST_CMD;